
CONTENTS OF THIS FILE
---------------------

 * About OG Mollom
 * Dependencies
 * Configuration and features

ABOUT OG MOLLOM
---------------

This module provides integration of OG with the Mollom module. Currently only
allows bypassing Mollom protection on group content forms that are protected by
Mollom.

DEPENDENCIES
------------

This module requires that cron is running.

 * OG 7.x-2.x (http://drupal.org/project/og)
 * Mollom 7.x-2.x (http://drupal.org/project/mollom)

CONFIGURATION AND FEATURES
--------------------------

When you create a new Group you can grant the "Bypass mollom protection"
permission to the appropriate roles from the OG Permissions page
(/admin/config/group/permissions).
